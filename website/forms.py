from django import forms
from .models import Contact, Group
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _


class CreateAccountForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['username', 'email', 'password']

        widgets = {
            'password': forms.PasswordInput(attrs={'required': True}),
        }

    def save(self, commit=True):
        if self.errors:
            raise ValueError(_('Invalid data'))
        instance = get_user_model().objects.create_user(**self.cleaned_data)
        return instance


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        exclude = ['user']


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        exclude = ['user']
