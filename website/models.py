from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Group(models.Model):
    """
    A contact Group, such as Family, Friends, Work...

    `user` stores the User that create this Group, in other words,
    this `Group` belongs to `user`.
    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('User'),
    )

    name = models.CharField(
        _('Group name'),
        max_length=64,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Group')


class Contact(models.Model):
    """
    `Contact` is a simple class to store a contact with the fields:
        required fields: name, phone1
        optional fields: phone2, email, birthday

    `user` stores the User that create this Contact, in other words,
    this `Contact` belongs to `user`.
    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('User'),
    )

    name = models.CharField(
        _('Full name'),
        max_length=254,
    )

    phone1 = models.CharField(
        _('Phone 1'),
        max_length=28,
    )

    phone2 = models.CharField(
        _('Phone 2'),
        max_length=28,
        blank=True,
        null=True,
    )

    email = models.EmailField(
        _('E-mail'),
        blank=True,
        null=True,
    )

    birthday = models.DateField(
        _('Birthday'),
        blank=True,
        null=True,
    )

    groups = models.ManyToManyField(
        Group,
        verbose_name=_('Groups'),
        related_name='groups_set',
        blank=True,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('User')
