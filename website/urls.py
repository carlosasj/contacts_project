from django.conf.urls import url

from . import views


urlpatterns = [
    url(
        r'^$',
        views.index,
        name='index'
    ),
    url(
        r'^create-account/$',
        views.create_account,
        name='create_account'
    ),
    url(
        r'^logout/$',
        views.logout_view,
        name='logout'
    ),
    url(
        r'^contacts/new/$',
        views.edit_contact,
        name='new_contact'
    ),
    url(
        r'^contacts/edit/(\d+)/$',
        views.edit_contact,
        name='edit_contact'
    ),
    url(
        r'^groups/$',
        views.groups_overview,
        name='groups_overview'
    ),
    url(
        r'^groups/new/$',
        views.edit_group,
        name='new_group'
    ),
    url(
        r'^groups/edit/(\d+)/$',
        views.edit_group,
        name='edit_group'
    ),
]
