from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.utils.translation import ugettext_lazy as _
from .forms import CreateAccountForm, ContactForm, GroupForm
from .models import Contact, Group
from django.db.models.functions import Lower


def commom_cud(request, form_class, model_class, pk):
    '''
    Commom CUD is a function with the commom operations on the views for
    Create, Update and Delete.

    @request     = request
    @form_class  = a class extended from `forms.Form`
    @model_class = a class extended from `models.Model`
    @pk          = a primary key for `model_class`

    Returns:
    [action, instance, form]
    @action   = What the user apparentry wants to do ('delete', 'update',
                'none'); In case of 'none' you can assume that
                `not request.method == 'POST'`, and 'create' is merged with
                'update'
    @instance = The instance of `model_class` with `pk=pk`
    @form     = The instance of `form_class` with `request.POST` data loaded
    '''

    # Check if the `model_class` have a instance with the `pk`. If so, it's
    # stored in `instance`, and `action` is set.
    try:
        instance = model_class.objects.get(pk=pk)
    except model_class.DoesNotExist:
        instance = None

    if request.method == 'POST':    # Form send

        # IF instance exists AND action == 'delete'
        if instance and 'delete' in request.POST:
            return ('delete', instance, None)

        # get the Form with the `request.POST` data, and link it to `instance`
        # if `not instance == None`
        form = form_class(request.POST, instance=instance)

        if form.is_valid():
            return ('update', instance, form)

    else:    # Regular GET
        form = form_class(instance=instance)

    return ('none', instance, form)


def create_account(request):
    if request.method == 'POST':
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, _('Account successfully created. '
                                        'Please login.'))
            return redirect('index')
    else:
        form = CreateAccountForm()

    context = {
        'form': form,
    }
    return render(request, 'website/create_account.html', context)


@login_required
def index(request):
    '''
    Lists contacts from the request user
    '''

    context = {
        'contacts': request.user.contact_set.order_by(Lower('name')),
    }
    return render(request, 'website/list_contacts.html', context)


@login_required
def edit_contact(request, pk=None):
    '''
    Creates a new contact, or edit a old one
    '''

    action, instance, form = commom_cud(request, ContactForm, Contact, pk)

    if action == 'delete':
        instance.delete()
        messages.warning(request, _("Contact successfully deleted"))
        return redirect('index')

    elif action == 'update':
        # Fake-save the form, so it creates a new instance (or reuse the
        # old one) with the data received by form.
        inst = form.save(commit=False)
        inst.user = request.user
        inst.save()  # Really save the instance
        form.save_m2m()  # Save the Many To Many Relationship

        messages.success(request, _("Contact successfully created"))
        return redirect('index')

    else:    # Regular GET
        choices = request.user.group_set.all()
        form.fields['groups'].choices = [(c.id, c.name) for c in choices]
        context = {
            'form': form,
            'instance': instance,
            'verbose_name': (Contact
                             ._meta.verbose_name.title())
        }
        return render(request, 'website/edit.html', context)


@login_required
def groups_overview(request):
    '''
    Lists groups from the request user
    '''

    context = {
        'groups': request.user.group_set.order_by(Lower('name')),
    }
    return render(request, 'website/list_groups.html', context)


@login_required
def edit_group(request, pk=None):
    '''
    Creates a new group, or edit a old one
    '''

    action, instance, form = commom_cud(request, GroupForm, Group, pk)

    if action == 'delete':
        instance.delete()
        messages.warning(request, _("Group successfully deleted"))
        return redirect('groups_overview')

    elif action == 'update':
        # Fake-save the form, so it creates a new instance (or reuse the
        # old one) with the data received by form.
        inst = form.save(commit=False)
        inst.user = request.user
        inst.save()  # Really save the instance

        messages.success(request, _("Group successfully created"))
        return redirect('groups_overview')

    else:    # Regular GET
        context = {
            'form': form,
            'instance': instance,
            'verbose_name': (Group
                             ._meta.verbose_name.title())
        }
        return render(request, 'website/edit.html', context)


def logout_view(request):
    logout(request)
    return redirect('index')
